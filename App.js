import React, {Component} from "react";
import { View, StyleSheet } from "react-native";
import {RootNav} from './navigation';
import { Provider } from "react-redux";
import store from "./store";
import { AppLoading } from "expo";
import { loadFonts } from "./styles/Fonts";

export default class App extends Component {
  state ={loaded:false}
  render() {
    if (!this.state.loaded) {
      return ( <AppLoading  startAsync={loadFonts}  onFinish={() => this.setState({loaded:true})} 
                onError={() => console.log("Loading Rejected")}  /> );
    }
  return (
    <Provider store={store}>
      <RootNav/>
      </Provider>
  );
}}
