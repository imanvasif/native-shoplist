import React from 'react';
import { StyleSheet, View, Image,Text } from "react-native";
import COLORS from '../styles/Colors';
import { MenuButton } from '../components';

const DrawerMenu = (props) => {
  const {userName,avatarUrl} =props;
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          resizeMode='cover'
          source={{
            uri:
              (avatarUrl!=='' ) ? avatarUrl : "https://lh3.googleusercontent.com/proxy/ogY_mgvIFnLMjBPNEG9DocHdqIXcF_v8X5dOvv3SwTeEGssmp3kOC4yufwscZgFJoRDrBXTYRY0S2xc0PXmW9b799CPrLumB0d7nQkLcISs8K2RekRDstEi8ighej2INfvvNm4ql0VgL_ShLLdR-oMLszDEWgtk",
          }}
          style={styles.userImg}
        />
        <Text style={styles.userText}>{userName}</Text>
      </View>
      <View style={styles.menuContainer}>
        <MenuButton onPress={() => props.navigation.navigate("CreateList")} style={
            styles.menuBtn,
            {marginBottom:32}} 
            fontSize={14} 
            color={COLORS.main} 
            title="ADD NEW LIST"/>

        <MenuButton onPress={() => props.navigation.navigate("HomeStackOneTime")} 
            style={styles.menuBtn} 
            fontSize={14} 
            color={COLORS.main} 
            title="ONE TIME LISTS"/>
        
        <MenuButton onPress={() => props.navigation.navigate("HomeStackRegular")} 
            style={styles.menuBtn} 
            fontSize={14} 
            color={COLORS.main} 
            title="REGULAR  LISTS"/>
        
        <MenuButton onPress={() => props.navigation.navigate("UserSettingsStack")} 
            style={styles.menuBtn} 
            fontSize={14} 
            color={COLORS.main} 
            title="USER SETTINGS"/>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
      backgroundColor: "white",
    },
  headerContainer: {
      flexDirection: "row",
      alignItems:'center',
      paddingHorizontal:16,
      height:73,
    },
  userImg: {
      width: 52,
      height: 50,
      borderColor: COLORS.main,
      borderRadius: 25,
      borderWidth: 3
    },
  userText: {
      fontSize: 24,
      fontFamily: "MontserratRegular",
      marginLeft: 22,
    },
  menuContainer: {
      height:'100%',
      padding:16,
      backgroundColor: COLORS.main,
      borderTopStartRadius: 18,
      borderTopEndRadius: 18,
    },
  menuBtn:{
      height:34,
      backgroundColor:'white',
      marginBottom:10}
});
export default DrawerMenu;

