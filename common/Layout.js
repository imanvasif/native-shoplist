import React from 'react'
import {View,StyleSheet } from 'react-native'
import COLORS from '../styles/colors';

const Layout = ({children}) => {
    return (
        <View style={styles.wrapperContainer}>
            <View style={styles.container}>{children}</View>
        </View>
        
    )
}
const styles = StyleSheet.create({
    wrapperContainer:{backgroundColor:COLORS.main,flex:1},
    container:{backgroundColor:'#fff',flex:1,borderTopStartRadius:20,borderTopEndRadius:20,padding:16},
});
export default Layout
