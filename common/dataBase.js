import { AsyncStorage } from 'react-native';
import store from '../store';

export const dataBase = async () => {
     try { await AsyncStorage.setItem('shoplist', JSON.stringify(store.getState()));} 
     catch (e) {console.log(e)} 

}