import React from 'react'
import {View,Text,StyleSheet } from 'react-native'
import COLORS from '../styles/colors'

export const ProgressBar = ({progressBarStatus}) => {
    return (
        <View style={styles.progressBarWrapper}>
            <View style={[styles.progressBar,{width:`${progressBarStatus}%`}]}></View>
        </View>
    )
}

const styles = StyleSheet.create({
    progressBarWrapper: {height:19,width:'100%',backgroundColor:'#eeeeee',borderColor:'#000',borderWidth:1,
    borderRadius:20,borderColor:"#fff"},
    progressBar:{height:19,backgroundColor:COLORS.secondary,borderColor:'#000',borderWidth:1,
    borderRadius:20,borderColor:"#fff"}
});