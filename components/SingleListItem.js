import React,{useState} from 'react'
import { View, Text,TouchableOpacity,StyleSheet } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'; 
import { AntDesign } from '@expo/vector-icons'; 
import COLORS from '../styles/Colors';

export const SingleListItem = ({shopItem,editMode,setEditData,onComplateSingleItemHandler,onDeleteSingleItemHandler}) => {
    const [done,setDone] = useState(shopItem.done)
    const setEditDataHandler =()=>{
        setEditData(shopItem);
    }
    const Container = editMode ? View : TouchableOpacity;
    React.useLayoutEffect(() => {
        setDone(shopItem.done);
      }, [shopItem]);

    return (
        <Container style={[styles.container,{opacity: editMode ? 1 : done ? 0.5 : 1}]}
                    onLongPress={()=>onComplateSingleItemHandler(shopItem.id)}>
            {editMode ? 
             <View style={styles.editIcon}><MaterialIcons onPress={setEditDataHandler} name="edit" size={24} color="white" /></View>
            : null}
            <View style={[styles.innerContainer,{width:editMode ? '81%' : "100%"} ]}>
                <Text style={styles.singleItemText}>{shopItem.name}</Text>
                <Text style={styles.singleItemText}>{`x${shopItem.count} ${shopItem.type}`}</Text>
            </View>
            {editMode ? 
                <View style={styles.deleteIcon}>
                    <AntDesign onPress={()=>onDeleteSingleItemHandler(shopItem.id,shopItem.name)} name="close" size={24} color="white" />
                </View>
                : null}
            
        </Container>
    )
}
const styles= StyleSheet.create({
    container:{
        flexDirection:'row',
        alignItems:'center',
        borderWidth:2,
        borderColor:COLORS.main,
        borderRadius:27,
        marginBottom:14,
        maxHeight:40
    },
    innerContainer:{
        height:40,
        alignItems:'center',
        width:'81%',
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal:19
    },
    singleItemText:{
        fontSize:14,
        fontFamily:'MontserratMedium',
        color:COLORS.black
    },
    deleteIcon:{
        backgroundColor:COLORS.main,
        borderRadius:18,
        height:36,
        width:36,
        justifyContent:'center',
        alignItems:'center'
    },
    editIcon:{
        backgroundColor:COLORS.redlight,
        borderRadius:18,
        height:36,
        width:36,
        justifyContent:'center',
        alignItems:'center'
    }
    
})
