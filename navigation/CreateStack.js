import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import COLORS from "../styles/Colors"
import { CreateScreen } from "../screens/CreateScreen";

const { Navigator, Screen } = createStackNavigator();

export const CreateStack = () => {
  return (
    <Navigator>
      <Screen name="New List" component={CreateScreen} options={{
        title:'New List',
        headerTitleAlign:'center',
        headerStyle:{backgroundColor:COLORS.main,height:73, elevation:0,shadowOpacity:0},
        headerTintColor:'#fff',
        headerTitleStyle: {fontFamily:'MontserratBold',fontWeight: '600',fontSize:18}}} />
    </Navigator>
  );
};
