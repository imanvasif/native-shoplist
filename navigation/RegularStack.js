import React from "react";
import { createStackNavigator, } from "@react-navigation/stack";
import COLORS from "../styles/Colors"
import { RegularScreen, SingleItemScreen } from "../screens";

const { Navigator, Screen } = createStackNavigator();

export const HomeStackRegular = () => (
  <Navigator screenOptions={{
    headerTitleAlign:'center',
    headerStyle:{
      backgroundColor:COLORS.main,
      height:73, 
      elevation:0,
      shadowOpacity:0
    },
    headerTintColor:'#fff',
    headerTitleStyle: {
      fontFamily:'MontserratMedium',
      fontWeight: '600',
      fontSize:18
    },
    headerTitleAlign:'center',}}>
    <Screen name="Regular" component={RegularScreen} options={{title:'Regular Lists',
      headerTitleStyle:{fontSize:16,fontWeight: '600',fontFamily:'MontserratMedium'}}} />
    <Screen name="SingleList" component={SingleItemScreen} 
      options={({route})=>({title:route.params.name})} />
  </Navigator>
);

