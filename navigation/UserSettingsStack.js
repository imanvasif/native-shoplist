import React from "react";
import { createStackNavigator, } from "@react-navigation/stack";
import { UserSettingsScreen } from "../screens";
import COLORS from '../styles/Colors'

const { Navigator, Screen } = createStackNavigator();

export const UserSettingsStack = () => {

  return (
    <Navigator>
      <Screen name="User Settings" component={UserSettingsScreen} options={{
        title:'User Settings',
        headerTitleAlign:'center',
        headerStyle:{backgroundColor:COLORS.main,height:73, elevation:0,shadowOpacity:0},
        headerTintColor:'#fff',
        headerTitleStyle: {fontFamily:'MontserratBold',fontWeight: '600',fontSize:18}}}
         />
    </Navigator>
  );
};
