import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { DrawerMenu } from "../common/DrawerMenu"
import { OneTimeStack } from "./OneTimeStack";
import { CreateStack } from "./CreateStack";
import { RegularStack } from "./RegularStack";
import { UserSettingsStack } from "./UserSettingsStack";
import { connect } from "react-redux";

const { Navigator, Screen } = createDrawerNavigator();
const mapStateToProps =(state) =>({userSettings:getUserSettings(state)});

export const RootNav = connect(mapStateToProps)((props) => {
  const {userName,avatarUrl} = props.userSettings;
  return (
  <NavigationContainer>
    <Navigator drawerContent={props => <DrawerMenu {...props} avatarUrl={avatarUrl} userName={userName}/>}>
      <Screen name="OneTimeStack" component={OneTimeStack} />
      <Screen name="CreateStack" component={CreateStack} />
      <Screen name="RegularStack" component={RegularStack} />
      <Screen name="UserSettingsStack" component={UserSettingsStack} />
    </Navigator>
  </NavigationContainer>
  )
  });
