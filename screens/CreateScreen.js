import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { MenuButton } from '../components';
import { addList } from '../store/shoplist'
import { StoreData } from '../common/dataBase';


export const CreateScreen = connect(null,{addList})(({addList,navigation}) => {
    const [fields,setFields] = useState({listType:'oneTimeList',name:''});
    const fieldsChangeHandler=(name,value)=>{setFields(()=>({...fields,[name]:value}))}
    const createUserHander=()=>{
        for(let key in fields){
            if(fields[key].trim() === ''){
                Alert.alert("Error!","Please write list name",[{ text: "OK"}],{ cancelable: false });
                return ;
            }
        }
        addList(fields);
        StoreData();
        alert(`${fields.name} added to ${fields.listType}`);
        setFields({listType:'oneTimeList',name:''});
        if(fields.listType === 'oneTimeList'){navigation.navigate('HomeStackOneTime')}
        else{navigation.navigate('HomeStackRegular')}    
        
    }
    return (
        <Layout>
            <Text style={styles.inputLabel}>List Name</Text>
            <TextInput onChangeText={(value)=>fieldsChangeHandler('name',value)} value={fields.name} style={styles.input}/>
            <View style={styles.listTypeWrapper}>
                <CustomBtn onPress={()=>fieldsChangeHandler("listType","oneTimeList")} style={[styles.listTypeBtn,{opacity:fields.listType === 'oneTimeList' ? 1 : 0.5}]} color={COLORS.black} fontSize={12} title="One Time"/>
                <CustomBtn onPress={()=>fieldsChangeHandler("listType","regularList")} style={[styles.listTypeBtn,{opacity:fields.listType === 'regularList' ? 1 : 0.5}]} color={COLORS.black} fontSize={12} title="Regular"/>
            </View>
            <CustomBtn onPress={createUserHander} style={{ marginTop: 12 }} title="CREATE LIST"/>
        </Layout>
    )
})
const styles = StyleSheet.create({  
    inputLabel:{
        textAlign:'center',
        fontFamily:'MontserratMedium',
        fontSize:12,color:COLORS.black,marginBottom:4},
        input:{backgroundColor:COLORS.gray,height:42,borderRadius:45,borderColor:'#fff',textAlign:'center',
        fontFamily:'MontserratMedium',fontSize:18},
        listTypeWrapper:{flexDirection:'row',justifyContent:'space-between',marginVertical:4},
        listTypeBtn:{marginTop: 10,width:'48%',backgroundColor:COLORS.gray}
});