import React from 'react'
import {TouchableOpacity,StyleSheet,FlatList,Alert  } from 'react-native'
import Layout from '../common/Layout';
import { dataBase } from '../../common/dataBase';
import { Feather } from '@expo/vector-icons'; 
import { ListItem } from '../components/ListItem';
import {connect} from "react-redux";
import {getOneTimeList, deleteList} from '../store/shoplist';

const mapStateToProps =(state) =>({oneTimeList:getOneTimeList(state)});

export const OneTimeLists = connect(mapStateToProps,{deleteList})( ({navigation,oneTimeList,deleteList}) => {
    React.useLayoutEffect(() => {
        navigation.setOptions({
          headerRight: () => (
              <TouchableOpacity style={{marginRight:10}} onPress={()=>navigation.openDrawer()}>
                  <Feather name="menu" size={35} color="white" />
              </TouchableOpacity>   
          ),
        });
      }, [navigation]);
      
      const deleteListHandler=(listType,listID,listName)=>{
        Alert.alert('',`Do you want delete ${listName} ?`,
            [{ text: 'No', onPress: () => alert(`Operation was deleted`)},
            { text: 'Ok', onPress: () => {deleteList({listType,listID});alert(`${listName} was deleted`);dataBase();}}],
            { cancelable: true }
          );  
      }
    return (
        <Layout>
            <FlatList 
                    data={oneTimeList} renderItem={({item})=> (
                        <TouchableOpacity onLongPress={()=>deleteListHandler("oneTimeList",item.id,item.name)}
                                          onPress={()=>navigation.navigate('SingleList',{listType:'oneTimeList',listID:item.id})}>
                            <ListItem listType={"oneTimeList"} key={item.id} list={item} />
                        </TouchableOpacity>
                    )}
                    keyExtractor={item => item.id}
                />
        </Layout>
                
    )
})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
