import React from 'react';
import {TouchableOpacity,StyleSheet,FlatList } from 'react-native'
import { connect } from 'react-redux';
import { dataBase } from '../common/dataBase';
import {getRegularList, deleteList,getLists} from '../store/shoplist';
import {ListItem} from '../components/ListItem';
import { Feather } from '@expo/vector-icons'; 


const mapStateToProps =(state) =>({regularList:getRegularList(state)});

export const RegularLists = connect(mapStateToProps,{deleteList,getLists})( ({navigation,regularList,deleteList,getLists}) => {
    useEffect(()=>{
        getLists();
    },[]);
    React.useLayoutEffect(() => {
        navigation.setOptions({
          headerRight: () => (
              <TouchableOpacity style={{marginRight:10}} onPress={()=>navigation.openDrawer()}>
                  <Feather name="menu" size={35} color="white" />
              </TouchableOpacity>   
          ),
        });
      }, [navigation]);
    const deleteListHandler=(listType,listID,listName)=>{
        Alert.alert('',`Do you want delete ${listName} ?`,
            [{ text: 'No', onPress: () => alert(`Operation was deleted`)},
            { text: 'Ok', onPress: () => {deleteList({listType,listID});alert(`${listName} was deleted`);StoreData();}}],
            { cancelable: true }
          );  
      }
    return (
        <Layout>
            <FlatList 
                    data={regularList} renderItem={({item})=> (
                        <TouchableOpacity onLongPress={()=>deleteListHandler("regularList",item.id,item.name)} onPress={()=>navigation.navigate('SingleList',{listType:'regularList',listID:item.id})}>
                            <ListItem listType={"regular"} key={item.id} list={item} />
                        </TouchableOpacity>
                    )}
                    keyExtractor={item => item.id}
                />
        </Layout>
                
    )
})
const styles = StyleSheet.create({
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
