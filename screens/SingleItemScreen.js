import React,{useState} from 'react'
import {View,Text,TouchableOpacity,FlatList,StyleSheet,TextInput,Alert } from 'react-native'
import Layout from '../common/Layout';
import { SingleListItem, CustomBtn, MenuButton } from '../components';
import COLORS from '../styles/Colors';
import { FontAwesome } from '@expo/vector-icons'; 
import { MaterialIcons } from '@expo/vector-icons'; 
import { Feather } from '@expo/vector-icons';

import { connect } from 'react-redux';
import { getRegularList,getOneTimeList,addListItem,updateListItem,deleteListItem,complateListItem,resetList } from '../store/shoplist';

import { dataBase } from '../common/dataBase';

const mapStateToProps=(state)=>({oneTimeList:getOneTimeList(state),regularList:getRegularList(state)})

export const SingleList = connect(mapStateToProps,{resetList,addListItem,updateListItem,deleteListItem,complateListItem})((props) => {

    const initialEditdata = {name:'',type:'litre',count:"0",id:''};
    const[editData,setEditData] = useState({name:'milk',type:'litre',count:"2",id:''});
    const [editMode,setEditMode] = useState(false);
    
    const singleList = props[props.route.params?.listType].find(list=>list.id === props.route.params?.listID);
    const shoplistCount = singleList.shoplist.length;
    const isCompleteCount =  singleList.shoplist.filter(singleList=>singleList.done === true).length;
    
    React.useLayoutEffect(() => {
        let headerRightIcon = editMode ? <Feather name="save" size={24} color="white" /> :
        <MaterialIcons 
            name="edit" 
            size={24} 
            color="white" />;
            props.navigation.setOptions({
                headerRight: () => (
                <TouchableOpacity style={{marginRight:10}} 
                                    onPress={()=>{setEditMode(!editMode),setEditData({name:'milk',type:'litre',count:"2",id:''});}}>
                    <Text>{headerRightIcon}</Text>
                </TouchableOpacity>    
          ),
        });
      }, [props.navigation, editMode]);
    
    const EditDataChangeHandler=(name,value)=>{setEditData(()=>({...editData,[name]:value}))}
    const EditDataCountPlusHandler=()=>{setEditData(()=>({
        ...editData,['count']:(++editData.count).toString()
}))}
    
const EditDataCountMinusHandler =()=>{
        if(editData.count !== '0')  setEditData(()=>({ ...editData,['count']:(--editData.count).toString()}))
    }
    const addSingleListHandler =()=>{
        if(editData.name.trim() === '' || +editData.count === 0){
            alert('Please write position name and and choose count');
        } else{
            props.addListItem({listType:props.route.params?.listType,type:editData.type,name:editData.name,count:editData.count,listID:props.route.params?.listID});
            dataBase();
            setEditData(initialEditdata);
        }
    };
    const canselSingleListHandler =()=>{
        setEditData(initialEditdata);
    };
    const editSingleListHandler =()=>{
        props.updateListItem({listType:props.route.params?.listType,type:editData.type,name:editData.name,count:editData.count,listID:props.route.params?.listID,itemID:editData.id});
        setEditData(initialEditdata);
        dataBase();
    };
    const onComplateSingleItemHandler=(itemID)=>{
        props.complateListItem({listType:props.route.params?.listType,listID:props.route.params?.listID,itemID:itemID});
        dataBase();
    }
    const onDeleteSingleItemHandler=(itemID,itemNAME)=>{
        Alert.alert('',`Do you want delete ${itemNAME} ?`,
            [{ text: 'No', onPress: () => alert(`Operation was deleted`)},
            { text: 'Ok', onPress: () => {props.deleteListItem({listType:props.route.params?.listType,listID:props.route.params?.listID,itemID:itemID});dataBase();}}],
            { cancelable: true }
          );  

    }
    const onResetListHandler=()=>{
        props.resetList({listType:props.route.params?.listType,listID:props.route.params?.listID});
        dataBase();
    }
    return (
        <Layout>
    { editMode ?  
        <View style={styles.editModeContainer}>
            <View style={styles.row}>
                <View style={styles.inputWrapper}>
                    <Text style={styles.inputLabel}>position name</Text>
                    <TextInput defaultValue={editData.name} onChangeText={(value)=>EditDataChangeHandler('name',value)} style={[styles.input,{fontSize:14}]}/>
                </View>
                <View style={styles.countWrapper}>
                    <Text style={styles.inputLabel}>count</Text>
                    <View style={styles.countButtonWrapper}>
                        <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <FontAwesome onPress={EditDataCountMinusHandler} name="minus" size={12} color="black" />
                        </TouchableOpacity>
                        <TextInput defaultValue={editData.count.toString()} onChangeText={(value)=>EditDataChangeHandler('count',value)} keyboardType="number-pad"  style={styles.input}/>
                        <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <FontAwesome onPress={EditDataCountPlusHandler} name="plus" size={12} color="black" />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.btnWrapper}></View>
            </View>
            <View style={styles.typeBtnWrapper}>
                <MenuButton onPress={()=>EditDataChangeHandler('type','pkg')} 
                            color={'#000000'} 
                            style={[styles.typeBtn,{opacity: editData.type === "pkg" ? 1 : 0.5}]} 
                            title="pkg"/>
                <MenuButton onPress={()=>EditDataChangeHandler('type','kg')} 
                            color={'#000000'} 
                            style={[styles.typeBtn,{opacity: editData.type === "kg" ? 1 : 0.5}]} 
                            title="kg"/>
                <MenuButton onPress={()=>EditDataChangeHandler('type','litre')} 
                            color={'#000000'} 
                            style={[styles.typeBtn,{opacity: editData.type === "litre" ? 1 : 0.5}]} 
                            title="litre"/>
                <MenuButton onPress={()=>EditDataChangeHandler('type','bott')} 
                            color={'#000000'} 
                            style={[styles.typeBtn,{opacity: editData.type === "bott" ? 1 : 0.5}]} 
                            title="bott"/>
            </View>
            
                {
                    editData.id ?
                        <View style={styles.typeBtnWrapper}>
                           
                            <MenuButton onPress={()=>canselSingleListHandler()} 
                                        style={styles.canselBtn} 
                                        title="CANSEL"/>
                            <MenuButton onPress={()=>editSingleListHandler()} 
                                        style={styles.editBtn} 
                                        title="UPDATE"/>
                        </View>
                    :
                        <View style={styles.typeBtnWrapper}>
                            <MenuButton onPress={addSingleListHandler} 
                                        title="ADD TO LIST"/>
                        </View>
                }
                
        </View>  
            : null
    }       
            <View style={styles.headerWrapper}>
                <View style={styles.resetBtnWrapper} >
                    {
                        props.route.params?.listType === 'regularList' ?
                            <CustomBtn onPress={onResetListHandler}  style={styles.resetBtn} fontSize={10} title="RESET"/>
                        : null
                    }
                </View>
                <Text>{`${isCompleteCount} / ${shoplistCount}`}</Text>
            </View>
            <FlatList 
                    data={singleList.shoplist} renderItem={({item})=> (
                        <SingleListItem 
                            onDeleteSingleItemHandler  =  {onDeleteSingleItemHandler} 
                            onComplateSingleItemHandler=  {onComplateSingleItemHandler} 
                            editData={editData} setEditData={setEditData} editMode={editMode} shopItem={item}
                        />
                    )}
                    keyExtractor={item => item.id}
                />
        </Layout>
    )
})

const styles = StyleSheet.create({  
    editModeContainer:{
        borderBottomWidth:2,
        borderBottomColor:'#e5e5e5',
        paddingBottom:20,
        marginBottom:20
    },
    headerWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginBottom:14
    },
    resetBtnWrapper:{
        width:72
    },
    resetBtn:{
        height:20
    },
    inputLabel:{
        textAlign:'center',
        fontFamily:'MontserratMedium',
        fontSize:12
        ,color:COLORS.black,
        marginBottom:4
    },
    input:{
        backgroundColor:COLORS.gray,
        height:42,
        borderRadius:45,
        borderColor:'#fff',
        textAlign:'center',
        fontFamily:'MontserratBold',
        fontSize:18,
        flex:1
    },
    row:{
        flexDirection:'row'
    },
    inputWrapper:{
        width:'71%'
    },
    countWrapper:{
        width:'25%',
        marginLeft:'4%'
    },
    countButtonWrapper:{
        backgroundColor:COLORS.gray,
        flexDirection:'row',
        alignItems:'center',
        borderRadius:45
    },
    typeBtnWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:14
    },
    typeBtn:{
        width:'20%',
        backgroundColor:COLORS.gray,
        opacity:0.5
    },
    editBtn:{
        width:'48%'
    },
    canselBtn:{
        width:'48%',
        opacity:0.5
    },
});

