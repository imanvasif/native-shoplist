import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { getUserSettings,updateUser } from '../store/shoplist';
import { dataBase } from '../common/dataBase';
import { MenuButton } from '../components';
import COLORS from '../styles/Colors';

export const UserSettingsScreen = () => {
    
    return (
        <View style={styles.container} >
            <Text style={styles.inputLabel}>username</Text>
            <TextInput onChangeText={(value)=>fieldsChangeHandler('userName',value)} value={fields.userName} style={styles.input}/>
            <Text style={[styles.inputLabel,{marginTop:10}]}>avatar url</Text>
            <TextInput onChangeText={(value)=>fieldsChangeHandler('avatarUrl',value)} value={fields.avatarUrl} style={styles.input}/>
            <MenuButton onPress={createUserHander} style={{ marginTop: 15 }} title="SAVE CHANGES"/>
        </View>
    )
};

const styles = StyleSheet.create({  
    inputLabel:{
        textAlign:'center',
        fontFamily:'MontserratMedium',
        fontSize:12,
        color:COLORS.black,
        marginBottom:4
    },
    input:{
        backgroundColor:COLORS.grey,
        height:42,
        borderRadius:45,
        borderColor:'#fff',
        textAlign:'center',
        fontFamily:'MontserratMedium',
        fontSize:18
    },
});