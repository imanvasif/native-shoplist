import { createStore, combineReducers, applyMiddleware } from "redux";
import Thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { shoplistReducer } from "./projects";

const rootReducer = combineReducers({
    shoplist:shoplistReducer
});

export const store = createStore(rootReducer,composeWithDevTools(applyMiddleware(Thunk)));

export default store;
