import { AsyncStorage } from 'react-native'


                                                                             //  ACTION TYPES

const ADD_LIST = "ADD_LIST";
const DELETE_LIST = "DELETE_LIST";
const EDIT_LIST = "EDIT_LIST";
const UPDATE_USER = "UPDATE_USER";
const ADD_LIST_ITEM = "ADD_LIST_ITEM";
const UPDATE_LIST_ITEM = "UPDATE_LIST_ITEM";
const DELETE_LIST_ITEM = "DELETE_LIST_ITEM";
const COMPLATE_LIST_ITEM = "COMPLATE_LIST_ITEM";
const RESET_LIST = "RESET_LIST";


                                                                                //  SELECTORS

const MODULE_NAME = "shoplist";
export const getRegularList = (state) => state[MODULE_NAME].regularList;
export const getOneTimeList = (state) => state[MODULE_NAME].oneTimeList;
export const getUserSettings = (state) => state[MODULE_NAME].userSettings;


                                                                                //  REDUCER shoplistReducer

const initialState = { 
    regularList: [],oneTimeList:[],
    userSettings: {userName:'Username',avatarUrl:''}
  };
  
  export function shoplistReducer(state = initialState, { type, payload }) {
    switch (type) {
      case EDIT_LIST:
      return{...state,...payload.shoplist}
      case UPDATE_USER:
        return {...state,userSettings:{userName:payload.userName,avatarUrl:payload.avatarUrl}}
      case ADD_LIST:
        return {...state,
          [payload.listType]:[
            ...state[payload.listType],{id: `${Math.random()}${Date.now()}`,name: payload.name,shoplist:[]}
          ]
        }
      case DELETE_LIST:
        return { 
          ...state,
          [payload.listType]:[
            ...state[payload.listType].filter(item=>item.id!==payload.listID)
          ] 
        };
       case RESET_LIST:
         return {...state,
          [payload.listType]:state[payload.listType].map((list)=>{
            if(list.id === payload.listID){ 
              return {
                ...list,
                shoplist:list.shoplist.map((shopItem)=>{
                   return {
                      ...shopItem,done:false
                    }
                })
              }
            } return list;
          })
        } 
      case ADD_LIST_ITEM:
        return {...state,
          [payload.listType]:state[payload.listType].map((list)=>{
            if(list.id === payload.listID){ 
              return {
                ...list,
                shoplist:[...list.shoplist,
                  {id:`${Math.random()}${Date.now()}`,name: payload.name,type:payload.type,count:payload.count,done:false}
                ]
              }
            } return list;
          })
        } 
        case UPDATE_LIST_ITEM:
          return {...state,
            [payload.listType]:state[payload.listType].map((list)=>{
              if(list.id === payload.listID){ 
                return {
                  ...list,
                  shoplist:list.shoplist.map((shopItem)=>{
                    if(shopItem.id === payload.itemID){
                     return {
                        ...shopItem, name: payload.name,type:payload.type,count:payload.count,done:false
                      }
                    }return shopItem;
                  })
                }
              } return list;
            })
          } 
      case DELETE_LIST_ITEM:
        return {...state,
          [payload.listType]:state[payload.listType].map((list)=>{
            if(list.id === payload.listID){ 
              return {
                ...list,
                shoplist:list.shoplist.filter((shopItem)=>shopItem.id!==payload.itemID)
              }
            } return list;
          })
        } 
      case COMPLATE_LIST_ITEM:
        return {...state,
          [payload.listType]:state[payload.listType].map((list)=>{
            if(list.id === payload.listID){ 
              return {
                ...list,
                shoplist:list.shoplist.map((shopItem)=>{
                  if(shopItem.id === payload.itemID){
                   return { ...shopItem, done:!shopItem.done }
                  }return shopItem;
                })
              }
            } return list;
          })
        } 
  
      default: return state;
    }
  }

                                                                                //  ACTION CREATORS  

export const deleteList = (payload) => ({ type: DELETE_LIST, payload });
export const addList = (payload) => ({ type: ADD_LIST, payload });
export const editList = (payload) => ({ type: EDIT_LIST, payload });
export const resetList = (payload) => ({ type: RESET_LIST, payload });
export const updateUser = (payload) => ({ type: UPDATE_USER, payload });
export const addListItem = (payload) => ({ type: ADD_LIST_ITEM, payload });
export const updateListItem = (payload) => ({ type: UPDATE_LIST_ITEM, payload });
export const deleteListItem = (payload) => ({ type: DELETE_LIST_ITEM, payload });
export const complateListItem = (payload) => ({ type: COMPLATE_LIST_ITEM, payload });



export const getLists=()=>{
  return async dispatch=>{ 
    try {const value = await AsyncStorage.getItem('shoplist');const parsedValue = JSON.parse(value);
    if (parsedValue !== null) { dispatch(setLists(parsedValue)) } } catch (e) { }
  }
}
