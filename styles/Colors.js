const COLORS = {
    main: "#FF7676",
    redlight: "#FFE194",
    yellow: "#FFD976",
    black: "#303234",
    grey: "#EEEEEE",
  };
  
  export default COLORS;